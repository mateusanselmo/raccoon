var products_list = [
    {
      "id": "35",
      "name": "Miniatura Homem de Ferro",
      "price": "R$ 50.00"
    },
    {
      "id": "30",
      "name": "Miniatura Magneto",
      "price": "R$ 57.00"
    },{
      "id": "25",
      "name": "Miniatura Super Homem",
      "price": "R$ 37.00"
    },{
      "id": "71",
      "name": "Miniatura Bernard",
      "price": "R$ 71.00"
    },{
      "id": "56",
      "name": "Miniatura Batman",
      "price": "R$ 23.00"
    },
    {
      "id": "56",
      "name": "Miniatura Darth Vader",
      "price": "R$ 89.00"
    }];


function fixPrice(list) {
  let new_list = list.map(item => {                             // Alteração item por item da variável 'products_list' em uma nova variável
    return item = {
        'id': item.id,
        'name': item.name,
        'item_price': Number(item.price.slice(3))               // Alteração de 'price' para 'item_price' e o seu valor para um valor numérico
    }
  });

  return new_list;
}


var new_products_list = fixPrice(products_list);

// Print
console.log(new_products_list);
