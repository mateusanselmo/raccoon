var cart_products = [{
    'product_id': 'IN2350',
    'product_name': 'Teclado Mecânico Rozar K600',
    'product_price': '650.00',
    'product_quantity': '1',
    'product_url': 'http://www.foo.com/p/IN2350',
    'product_category': 'peripherals',
    'product_brand': 'Rozar'
}, {
    'product_id': 'IN4566',
    'product_name': 'Monitor Gamer LED GL UltraWide 25"',
    'product_price': '1399.99',
    'product_quantity': '1',
    'product_url': 'http://www.foo.com/p/IN4566',
    'product_category': 'monitor',
    'product_brand': 'GL'
}, {
    'product_id': 'ES7112',
    'product_name': 'Pacote de Post-it Fenix 50 unidades',
    'product_price': '14.95',
    'product_quantity': '5',
    'product_url': 'http://www.foo.com/p/ES7112',
    'product_category': 'office',
    'product_brand': 'Fenix'
}, {
    'product_id': 'BR8810',
    'product_name': 'Miniatura Yoshi Haras',
    'product_price': '6.50',
    'product_quantity': '2',
    'product_url': 'http://www.foo.com/p/BR8810',
    'product_category': 'others',
    'product_brand': 'Haras'
}];


function cartTotal(acumulador, item, index, arr) {                                         // Função que retorna se há frete grátis e o valor total do carrinho
    if(index === arr.length - 1) {                                                         // Verifica se está na última iteração
        acumulador += (Number(item.product_price)*Number(item.product_quantity));
        if(acumulador > 300) {
            var freeShipping = true;                                                       // Frete grátis
        } else {
            var freeShipping = false;                                                      // Frete pago
        }
        return {'totalPrice':acumulador, 'freeShipping':freeShipping};                     // Retorno na última iteração
    }
    return acumulador + (Number(item.product_price)*Number(item.product_quantity));        // Retornos em iterações antes da última
}


var cartPricesInfo = cart_products.reduce(cartTotal, 0);                // Chamando a função que nos retorna totalPrice e freeshipping em uma variável
var totalPrice = cartPricesInfo.totalPrice;
var freeShipping = cartPricesInfo.freeShipping;

// Print
console.log(cartPricesInfo);
console.log(`totalPrice: ${totalPrice};\nfreeShiping: ${freeShipping};`);
